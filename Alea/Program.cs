﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alea {
    class Alea {
        byte[] values = new byte[20];

        public Alea() {
            Random rnd = new Random();
            for (byte i = 0; i < values.Length; i++) {
                values[i] = (byte) rnd.Next(0, 100);
            }
        }

        public override String ToString() {
            String ans = "";

            Array.Sort(values);

            foreach (byte b in values) {
                ans += b + " ";
            }

            return ans;
        }

        public String ToStringImpaire() {
            String ans = "";

            Array.Sort(values);

            foreach (byte b in values) {
                if (b%2 == 1) { ans += b + " "; }
            }

            return ans;
        }

        public byte Max() {
            byte ans = 0;

            foreach (byte b in values) {
                if (b > ans) { ans = b; }
            }

            return ans;
        }

        static void Main(string[] args) {
            Alea alea = new Alea();

            Console.WriteLine(alea);
            Console.WriteLine(alea.ToStringImpaire());
            Console.WriteLine(alea.Max());

            // Stops the program
            Console.ReadLine();
        }
    }
}
