﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouleurS {
    class Color {
        public byte r, g, b;
        public string name;

        public Color(byte r, byte g, byte b, String name) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.name = name;
        }
        public Color(byte r, byte g, byte b) : this(r, g, b, "") { }

        // Pour les valeurs invalides
        public Color(int r, int g, int b, String name) : this(ConvertInput(r), ConvertInput(g), ConvertInput(b), name) { }
        public Color(int r, int g, int b) : this(r, g, b, "") { }

        private static byte ConvertInput(int v) { return (v <= 256 && v > 0) ? Convert.ToByte(v) : (byte)0; }

        public static bool operator == (Color a, Color b) {
            return a.r == b.r && a.g == b.g && a.b == b.b && a.name == b.name;
        }

        public static bool operator !=(Color a, Color b) {
            return a.r != b.r || a.g != b.g || a.b != b.b || a.name != b.name;
        }

        public override string ToString() {
            return this.name + " #" + this.r.ToString("X2") + this.g.ToString("X2") + this.b.ToString("X2");
        }
    }

    class Colors {
        public ArrayList colors = new ArrayList();

        public void AddColor(Color c) {
            this.colors.Add(c);
        }

        public override string ToString() {
            string ans = "";

            foreach (Color c in this.colors) { ans += c + "\n"; }

            return ans;
        }

        public Color Search(String name) {
            foreach (Color c in this.colors) {
                if (c.name == name) {
                    return c;
                }
            }
            return new Color(0, 0, 0, "");
        }

        public void Delete(String name) {
            foreach (Color c in this.colors) {
                if (c.name == name) {
                    this.colors.Remove(c);
                    break;
                }
            }
        }

        public void Modify(String name, Color newColor) {
            foreach (Color c in this.colors) {
                if (c.name == name) {
                    this.colors[this.colors.IndexOf(c)] = newColor;
                    break;
                }
            }
        }

        static void Main(string[] args) {
            Colors colors = new Colors();
            colors.AddColor(new Color(212, 145, 255));
            colors.AddColor(new Color(0xff, 0xff, 0xff, "White"));
            colors.AddColor(new Color(23253, 1023, -123, "Yeeet"));
            colors.AddColor(new Color(100, 100, 100, "Dark Gray"));
            colors.AddColor(new Color(0, 0, 0, "Black"));

            Console.WriteLine("\nCouleurs");
            Console.Write(colors);

            colors.Delete("");
            colors.Delete("Yeeet");

            Console.WriteLine("\nCouleurs apres delete");
            Console.Write(colors);

            Console.WriteLine("\nTests recherche");
            Console.WriteLine(colors.Search("White"));
            Console.WriteLine(colors.Search("asdasdads"));
            Console.WriteLine(colors.Search(""));

            colors.Modify("Dark Gray", new Color(0x10, 0x10, 0x10, "Darker Gray"));

            Console.WriteLine("\nCouleurs apres modify");
            Console.Write(colors);

            Console.WriteLine("\nPartie 2");
            Color c1 = new Color(128, 128, 128, "gris");
            Color c2 = new Color(255, 255, 255, "blanc");
            c1 = c2;

            Console.WriteLine(c1);
            Console.WriteLine(c2);

            c1.name = "rose";

            Console.WriteLine(c1);
            Console.WriteLine(c2);

            Console.Read(); // Arrete la console
        }
    }
}
