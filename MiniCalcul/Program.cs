﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniCalcul {
    class Program {
        enum Operators {
            add = '+',
            sub = '-',
            mul = '*',
            div = '/'
        }

        static void Main(string[] args) {
            while (true) {
                
                char c = '+';
                try {
                    Console.Write("Operateur: ");
                    c = Console.ReadLine()[0];
                } catch { Console.WriteLine("Veuillez entre un operateur"); }

                float param1, param2;
                param1 = param2 = 0;

                try {
                    Console.Write(" Param1: ");
                    param1 = float.Parse(Console.ReadLine());

                    Console.Write(" Param2: ");
                    param2 = float.Parse(Console.ReadLine());
                } catch { Console.WriteLine("Parametre n'est pas un nombre"); }

                float res = 0;
                switch (c) {
                    case (char) Operators.add:
                        res = param1 + param2;
                        break;

                    case (char) Operators.sub:
                        res = param1 - param2;
                        break;

                    case (char) Operators.mul:
                        res = param1 * param2;
                        break;

                    case (char) Operators.div:
                        res = param1 / param2;
                        break;

                    default:

                        Console.WriteLine("Operateur inexistant");
                        break;
                }

                Console.WriteLine(res);
            }
        }
    }
}
