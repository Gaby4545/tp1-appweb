﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palindrome {
    class Program {
        static public Boolean IsPalindrome(string palindrome) {
            for (int i = 0; i < palindrome.Length / 2; i++) {
                if (palindrome[i] != palindrome[palindrome.Length - i - 1]) {
                    return false;
                }
            }

            return true;
        }

        static void Main(string[] args) {
            while (true) {
                Console.Write("Entree un Palindrome: ");
                string palindrome = Console.ReadLine();

                if (IsPalindrome(palindrome)) {
                    Console.WriteLine("est un palindrome");
                } else {
                    Console.WriteLine("n'est pas un palindrome");
                }

                Console.Write("Voulez vous en entre un autre? y/n ");
                String Confirmation = Console.ReadLine();
                if (Confirmation == "y" || Confirmation == "" ) {
                    Console.Clear();
                } else {
                    break;
                }
            }
            
        }
    }
}
