﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Produit {
    [Serializable]
    public class ProduitP {
        private int noProduit;
        public int NoProduit {
            get { return noProduit; }
            set { noProduit = value; }
        }

        private string nomProduit;
        public string NomProduit {
            get { return nomProduit; }
            set { nomProduit = value; }
        }

        private string description;
        public string Description {
            get { return description; }
            set { description = value; }
        }

        private float prix;
        public float Prix {
            get { return prix; }
            set { prix = value >= 0f && value <= 500f ? value : 0f; }
        }

        private int qty;
        public int Quantity {
            get { return qty; }
            set { qty = value >= 0 && value <= 200 ? value : 0; }
        }

        private int qtyRupture;
        public int QteRupture {
            get { return qtyRupture; }
            set { qtyRupture = value >= 0 && value <= Quantity*.2f ? value : 0; }
        }

        private bool taxable;
        public bool Taxable {
            get { return taxable; }
            set { taxable = value; }
        }

        public float Total {
            get { return Prix * Quantity; }
        }

        public ProduitP(int noProduit, string nomProduit) {
            this.NoProduit = noProduit;
            this.NomProduit = nomProduit;
            this.Description = "";
            this.Quantity = 0;
            this.QteRupture = 0;
            this.Taxable = false;
        }

        public ProduitP() : this(0, "") {}

        public static bool operator ==(ProduitP a, ProduitP b) {
            return a.noProduit == b.noProduit;
        }

        public static bool operator !=(ProduitP a, ProduitP b) {
            return a.noProduit != b.noProduit;
        }

        public static bool operator >(ProduitP a, ProduitP b) {
            return (a.Quantity - a.QteRupture) > (b.Quantity - b.QteRupture);
        }

        public static bool operator <(ProduitP a, ProduitP b) {
            return (a.Quantity - a.QteRupture) < (b.Quantity - b.QteRupture);
        }

        public override string ToString() {
            string ans = "";

            ans += "noProduit " + this.NoProduit + "\n";
            ans += "nomProduit " + this.NomProduit + "\n";

            if(this.Description != "") ans += "Desc " + this.Description + "\n";
            if(this.Quantity > 0) ans += "Qty " + this.Quantity + "\n";
            if(this.QteRupture > 0) ans += "QtyRupture " + this.QteRupture + "\n";
            ans += "Taxable " + this.Taxable + "\n";

            return ans;
        }

        static void Main(string[] args) {
        }
    }
}
