﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ProduitTest {
    [TestClass]
    public class UnitTest1 {
        Produit.ProduitP p1;
        Produit.ProduitP p2;

        [TestInitialize]
        public void TestInitialize() {
            p1 = new Produit.ProduitP(0, "Pomme");
            p1.Quantity = 43;
            p1.QteRupture = 20;
            p1.Prix = 1.99f;

            p2 = new Produit.ProduitP(1, "Poire");
            p2.Quantity = 28;
            p2.Prix = 2.99f;
        }

        [TestMethod]
        public void ProduitSetQty() {
            p1.Quantity = 134;
            Assert.IsTrue(p1.Quantity == 134);
        }

        [TestMethod]
        public void ProduitSetQtyTooHigh() {
            p1.Quantity = 560;
            Assert.IsTrue(p1.Quantity == 0);
        }

        [TestMethod]
        public void ProduitSetQtyTooLow() {
            p1.Quantity = -102;
            Assert.IsTrue(p1.Quantity == 0);
        }

        [TestMethod]
        public void ProduitSetPrice() {
            p1.Prix = 134.76f;
            Assert.IsTrue(p1.Prix == 134.76f);
        }

        [TestMethod]
        public void ProduitSetPriceTooHigh() {
            p1.Prix = 560.34f;
            Assert.IsTrue(p1.Prix == 0);
        }

        [TestMethod]
        public void ProduitSetPriceTooLow() {
            p1.Prix = -102.30f;
            Assert.IsTrue(p1.Prix == 0);
        }

        [TestMethod]
        public void ProduitEqualOperator() {
            Assert.IsFalse(p1 == p2);
        }

        [TestMethod]
        public void ProduitNotEqualOperator() {
            Assert.IsTrue(p1 != p2);
        }

        [TestMethod]
        public void ProduitBiggerOperator() {
            Assert.IsTrue(p1 > p2);
        }

        [TestMethod]
        public void ProduitSmallerOperator() {
            Assert.IsFalse(p1 < p2);
        }

        [TestMethod]
        public void ProduitGetTotal() {
            Assert.IsTrue(p1.Total == 85.57f);
        }
    }
}
