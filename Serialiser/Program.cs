﻿using Produit;

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

namespace Serialiser {
    class Serialize {

        public static void SerializeProd(List<ProduitP> arr, String filePath) {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None);

            foreach (object obj in arr) {
                formatter.Serialize(stream, obj);
            }

            stream.Close();
        }

        public static ProduitP DeSerializeProd(String filePath) {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            ProduitP p = (ProduitP) formatter.Deserialize(stream);
            stream.Close();

            return p;
        }

        public static void SerializeProdXml(List<ProduitP> arr, String filePath) {
            XmlSerializer ser = new XmlSerializer(typeof(ProduitP));
            TextWriter writer = new StreamWriter(filePath);

            foreach (object obj in arr) {
                ser.Serialize(writer, obj);
            }

            writer.Close();
        }

        public static void ReadFile(string filePath) {
            string text = File.ReadAllText(filePath);
            Console.WriteLine(text);
        }



        static void Main(string[] args) {
            List<ProduitP> p = new List<ProduitP>();

            p.Add(new ProduitP(1, "Patate"));
            p.Add(new ProduitP(2, "Tomate"));

            SerializeProd(p, "C:\\Users\\g845\\Documents\\yeet.bin");
            SerializeProdXml(p, "C:\\Users\\g845\\Documents\\yeet.xml");
            //Console.Write(DeSerializeProd("C:\\Users\\g845\\Documents\\yeet.bin"));

            ReadFile("C:\\Users\\g845\\Documents\\yeet.bin");
            Console.Write("\nXml\n");

            ReadFile("C:\\Users\\g845\\Documents\\yeet.xml");

            Console.Read();
        }
    }
}
