﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TestDotNet {
    class ConvertionTemperatures {

        float[] GetConvertFormula(char unit) {
            float[] formula = {1, 0}; // Valeurs par default qui ne font rien

            switch (unit) {
                case ('f'):
                case ('F'):
                    //Console.WriteLine("Fahreinheit");
                    formula[0] = 5f/9f; // A multiplier/diviser
                    formula[1] = 32f; // A ajoute/soustraire
                    break;

                case ('c'):
                case ('C'):
                    //Console.WriteLine("Celsius");

                    break;

                case ('k'):
                case ('K'):
                    //Console.WriteLine("Kelvin");

                    formula[1] = 273.15f;
                    break;

                default:
                    //Console.WriteLine("Unite invalide ou inexistante");

                    return null;
            }
            return formula;
        }

        string ConvertUnits(char unitFrom, char unitTo, int value) {

            // Convertie en Celsius
            float[] formula = GetConvertFormula(unitFrom);
            if (formula == null) { return "Premiere unite incorecte"; }
            float result = (value - formula[1]) * formula[0];

            // Convertie de celsius
            formula = GetConvertFormula(unitTo);
            if (formula == null) { return "Deuxieme unite incorecte"; }
            result = (result / formula[0]) - formula[1];

            return result + " " + unitTo;
        }

        void ConvertUnits() {
            Console.WriteLine("Convertion:  (Exemple: 20f vers c)");
            // Formule de convertion
            String Formule = Console.ReadLine();

            // Retire les espaces
            Regex SpaceRgx = new Regex(@" ");
            Formule = SpaceRgx.Replace(Formule, "");

            // Cherche le mot cle et separe 
            Regex SplitRgx = new Regex(@"to|vers");
            string[] values = SplitRgx.Split(Formule);

            if (values.Length == 2) {
                // Fait la conversion
                Regex Rgx = new Regex(@"\D$");
                Console.WriteLine(ConvertUnits(values[0][values[0].Length - 1], values[1][0], int.Parse(Rgx.Replace(values[0], ""))));
            } else {
                Console.WriteLine("Formule Incorrecte");
            }
        }

        static void Main(string[] args) {
            ConvertionTemperatures c = new ConvertionTemperatures();
            while (true) {
                c.ConvertUnits();
            }
        }
    }
}
