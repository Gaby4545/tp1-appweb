﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Texte {
    class Program {
        static void Main(string[] args) {
            while (true) {
                Console.WriteLine("Entrez une phrase :");
                String s = Console.ReadLine();

                Console.WriteLine("Nombre de majuscules " + Regex.Matches(s, @"[A-Z]").Count);
                Console.WriteLine("Nombre de ponctuations " + Regex.Matches(s, @"[.,':;!?]").Count);
                Console.WriteLine("Nombre de characteres " + s.Length);
            }
        }
    }
}
